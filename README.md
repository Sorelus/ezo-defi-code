# Ezo Defi Code

Bonjour à l'équipe EZO.

Pour cloner le projet, c'est simple : git clone https://gitlab.com/Sorelus/ezo-defi-code.git

Lorsque le projet sera cloné, basculez sur la branch develop, c'est là que vous verez ma solution du defi.

Merci.

Clebert Sorel M.